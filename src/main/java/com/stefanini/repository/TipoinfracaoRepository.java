package com.stefanini.repository;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.stefanini.model.Tipoinfracao;

public class TipoinfracaoRepository {
	@Inject
	private EntityManager manager;

	public Tipoinfracao buscar(Tipoinfracao tipoinfracao) {
		return this.manager.find(Tipoinfracao.class, tipoinfracao.getIdTipoInfracao());
	}
	
	public void inserir(Tipoinfracao tipoinfracao){
		this.manager.persist(tipoinfracao);
	}
	
	public List<Tipoinfracao> listar(){
		return this.manager.createQuery("select a from Tipoinfracao a", Tipoinfracao.class).getResultList();
	}
}
