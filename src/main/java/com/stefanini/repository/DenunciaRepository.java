package com.stefanini.repository;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.stefanini.model.Denuncia;

public class DenunciaRepository {
	
	@Inject 
	private EntityManager manager;
	
	public Denuncia buscar(Denuncia denuncia) {
		return this.manager.find(Denuncia.class, denuncia.getId());
	}
	
	public void inserir(Denuncia denuncia){
		this.manager.persist(denuncia);
	}
	
	public List<Denuncia> listar(){
		return this.manager.createQuery("select a from Agente a", Denuncia.class).getResultList();
	}

}
