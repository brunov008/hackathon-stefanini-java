package com.stefanini.repository;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.stefanini.model.Localinfracao;

public class LocalinfracaoRepository{
	@Inject
	private EntityManager manager;

	public Localinfracao buscar(Localinfracao localinfracao) {
		return this.manager.find(Localinfracao.class, localinfracao.getIdLocalInfracao());
	}
	
	public void incluirLocainfracao(Localinfracao localinfracao){
		this.manager.persist(localinfracao);
	}
	
	public List<Localinfracao> listar(){
		return this.manager.createQuery("select a from Localinfracao a", Localinfracao.class).getResultList();
	}
}
