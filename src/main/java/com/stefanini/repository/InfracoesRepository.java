package com.stefanini.repository;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.stefanini.model.Infracoes;

public class InfracoesRepository {

	@Inject
	private EntityManager manager;

	public Infracoes buscar(Infracoes infracoes) {
		return this.manager.find(Infracoes.class, infracoes.getIdInfracao());
	}
	
	public void inserir(Infracoes infracoes){
		this.manager.persist(infracoes);
	}
	
	public List<Infracoes> Listar(){
		return this.manager.createQuery("select a from Infracoes", Infracoes.class).getResultList();
		
	}
}
