package com.stefanini.repository;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.stefanini.model.Proprietario;

public class ProprietarioRepository {
	@Inject
	private EntityManager manager;

	public Proprietario buscar(Proprietario proprietario) {
		return this.manager.find(Proprietario.class, proprietario.getCpfProprietario());
	}
	
	public void inserir(Proprietario proprietario){
		this.manager.persist(proprietario);
	}
	
	public List<Proprietario> Listar(){
		return this.manager.createQuery("select a from Proprietario a", Proprietario.class).getResultList();
	}
}
