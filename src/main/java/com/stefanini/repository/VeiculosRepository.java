package com.stefanini.repository;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.stefanini.model.Veiculos;

public class VeiculosRepository {

	@Inject
	private EntityManager manager;

	public Veiculos buscar(Veiculos veiculos) {
		return this.manager.find(Veiculos.class, veiculos.getPlaca());
	}
	
	public void inserir(Veiculos veiculos){
		this.manager.persist(veiculos);
	}
	
	public List<Veiculos> listar(){
		return this.manager.createQuery("select c from Veiculos c", Veiculos.class).getResultList();
	}
}
