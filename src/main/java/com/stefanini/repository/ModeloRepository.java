package com.stefanini.repository;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.stefanini.model.Modelo;

public class ModeloRepository {
	
	@Inject
	private EntityManager manager;

	public Modelo buscar(Modelo modelo) {
		return this.manager.find(Modelo.class, modelo.getIdModelo());
	}
	
	public void inserir(Modelo modelo){
		this.manager.persist(modelo);
	}
	
	public List<Modelo> listar(){
		return this.manager.createQuery("select a from Modelo a", Modelo.class).getResultList();
	}
}
