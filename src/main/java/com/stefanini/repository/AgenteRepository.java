package com.stefanini.repository;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.stefanini.model.Agente;

public class AgenteRepository {
	@Inject
	private EntityManager manager;

	public Agente buscar(Agente agente) {
		return this.manager.find(Agente.class, agente.getIdAgente());
	}
	
	public void inserir(Agente agente){
		this.manager.persist(agente);
	}
	
	public List<Agente> listar(){
		return this.manager.createQuery("select a from Agente a", Agente.class).getResultList();
	}
}
