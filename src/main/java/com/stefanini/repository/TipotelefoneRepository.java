package com.stefanini.repository;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.stefanini.model.Tipotelefone;

public class TipotelefoneRepository {
	
	@Inject
	private EntityManager manager;

	public Tipotelefone buscar(Tipotelefone tipotelefone) {
		return this.manager.find(Tipotelefone.class, tipotelefone.getIdTipoTelefone());
	}


}
