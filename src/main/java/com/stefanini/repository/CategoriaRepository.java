package com.stefanini.repository;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.stefanini.model.Categoria;

public class CategoriaRepository {
	@Inject
	private EntityManager manager;
	
	public Categoria buscar(Categoria categoria) {
		return this.manager.find(Categoria.class, categoria.getIdCategoria());
	}
	
	public void inserir(Categoria categoria){
		this.manager.persist(categoria);
	}
	
	public List<Categoria> listar(){
		return this.manager.createQuery("select a from Categoria a", Categoria.class).getResultList();
	}
}
