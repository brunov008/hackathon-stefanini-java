package com.stefanini.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.stefanini.model.Denuncia;
import com.stefanini.repository.DenunciaRepository;

@Stateless
public class DenunciaService {
	
	@Inject
	private DenunciaRepository denunciaRepository;
	
	 @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	    public void buscarAgente(Denuncia denuncia){
	        denunciaRepository.buscar(denuncia);
	    }
	 
	 public void incluirAgente(Denuncia denuncia){
	    denunciaRepository.inserir(denuncia);
	 }
	    
	 public List<Denuncia> listarDenuncia(){
	   return denunciaRepository.listar();
	 }
}
