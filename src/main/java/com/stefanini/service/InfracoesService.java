package com.stefanini.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.stefanini.model.Infracoes;
import com.stefanini.repository.InfracoesRepository;

@Stateless
public class InfracoesService {
	 	@Inject
	    private InfracoesRepository infracoesRepository;

	    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	    public void buscar(Infracoes infracoes){
	        infracoesRepository.buscar(infracoes);
	    }
	    
	    public void incluirInfracoes(Infracoes infracoes){
	    	infracoesRepository.inserir(infracoes);
	    }
	    
	    public List<Infracoes> Listarinfracoes(){
	    	return infracoesRepository.Listar();
	    }
}
