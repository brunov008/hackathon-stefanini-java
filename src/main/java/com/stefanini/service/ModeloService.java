package com.stefanini.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.stefanini.model.Modelo;
import com.stefanini.repository.ModeloRepository;


@Stateless
public class ModeloService {
	
	@Inject
    private ModeloRepository modeloRepository;

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void buscar(Modelo modelo){
        modeloRepository.buscar(modelo);
    }
    
    public void incluirModelo(Modelo modelo){
    	modeloRepository.inserir(modelo);
    }
    
    public List<Modelo> listarModelo(){
    	return modeloRepository.listar();
    }

}
