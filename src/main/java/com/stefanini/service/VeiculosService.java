package com.stefanini.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import com.stefanini.model.Veiculos;
import com.stefanini.repository.VeiculosRepository;

@Stateless
public class VeiculosService {
	@Inject
    private VeiculosRepository veiculosRepository;

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void buscar(Veiculos veiculos){
        veiculosRepository.buscar(veiculos);
    }
    
    public void incluirveiculos(Veiculos veiculos){
    	veiculosRepository.inserir(veiculos);
    }
    
    public List<Veiculos> listarVeiculos(){
    	return veiculosRepository.listar();
    }
}
