package com.stefanini.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.stefanini.model.Localinfracao;
import com.stefanini.repository.LocalinfracaoRepository;

@Stateless
public class LocalinfracaoService {
		
		@Inject
	    private LocalinfracaoRepository localinfracaoRepository;

	    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	    public void buscar(Localinfracao localinfracao){
	        localinfracaoRepository.buscar(localinfracao);
	    }
	    
	    public void incluirLocalinfracao(Localinfracao localinfracao){
	    	localinfracaoRepository.incluirLocainfracao(localinfracao);
	    }
	    
	    public List<Localinfracao> listarLocalinfracao(){
	    	return localinfracaoRepository.listar();
	    }

}
