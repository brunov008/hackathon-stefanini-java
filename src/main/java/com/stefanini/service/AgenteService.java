package com.stefanini.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.stefanini.model.Agente;
import com.stefanini.repository.AgenteRepository;


@Stateless
public class AgenteService {
	@Inject
    private AgenteRepository agenteRepository;

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void buscarAgente(Agente agente){
        agenteRepository.buscar(agente);
    }
    
    public void incluirAgente(Agente agente){
    	agenteRepository.inserir(agente);
    }
    
    public List<Agente> listarAgente(){
    	return agenteRepository.listar();
    }
}
