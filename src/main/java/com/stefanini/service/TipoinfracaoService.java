package com.stefanini.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.stefanini.model.Tipoinfracao;
import com.stefanini.repository.TipoinfracaoRepository;

@Stateless
public class TipoinfracaoService {
	@Inject
    private TipoinfracaoRepository tipoinfracaoRepository;

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void buscar(Tipoinfracao tipoinfracao){
       tipoinfracaoRepository.buscar(tipoinfracao);
    }
    
    public void inserirTipoinfracao(Tipoinfracao tipoinfracao){
    	tipoinfracaoRepository.inserir(tipoinfracao);
    }
    
    public List<Tipoinfracao> listarTipoinfracao(){
    	return tipoinfracaoRepository.listar();
    }

}
