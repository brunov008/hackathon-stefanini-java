package com.stefanini.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.stefanini.model.Categoria;
import com.stefanini.repository.CategoriaRepository;

@Stateless
public class CategoriaService {
	@Inject
	private CategoriaRepository categoriaRepository;
	
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void buscarCategoria(Categoria categoria){
        categoriaRepository.buscar(categoria);
    }
    
    public void incluirCategoria(Categoria categoria){
    	categoriaRepository.inserir(categoria);
    }
    
    public List<Categoria> listarCategoria(){
    	return categoriaRepository.listar();
    }
}
