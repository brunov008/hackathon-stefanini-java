package com.stefanini.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.stefanini.model.Proprietario;
import com.stefanini.repository.ProprietarioRepository;

@Stateless
public class ProprietarioService {
	
	@Inject
	private ProprietarioRepository proprietarioRepository;

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void buscar(Proprietario proprietario){
        proprietarioRepository.buscar(proprietario);
    }
    
    public void inserirProprietario(Proprietario proprietario){
    	proprietarioRepository.inserir(proprietario);
    }
    
    public List<Proprietario> listarProprietario(){
    	return this.proprietarioRepository.Listar();
    }
}
