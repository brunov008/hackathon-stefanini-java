package com.stefanini.bean;


import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.stefanini.model.Denuncia;
import com.stefanini.service.DenunciaService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named("denunciaMB")
@SessionScoped
public class DenunciaBean implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
    private DenunciaService denunciaService;
    
    @Inject
    private Denuncia denuncia;
    
    private List<Denuncia> denuncias;
    
    public Denuncia getDenuncia() {
    	if(denuncia == null){
    		denuncia = new Denuncia();
    	}
		return denuncia;
	}

	public String chamarDenuncia() {
        return "/pages/teste.faces?faces-redirect=true";
    }

	public void setDenuncia(Denuncia denuncia) {
		this.denuncia = denuncia;
	}

	public List<Denuncia> getDenuncias() {
		if(denuncias == null){
			denuncias = new ArrayList<Denuncia>();
			denuncias = denunciaService.listarDenuncia();
		}
		return denuncias;
	}

	public void setDenuncias(List<Denuncia> denuncias) {
		this.denuncias = denuncias;
	}

}
