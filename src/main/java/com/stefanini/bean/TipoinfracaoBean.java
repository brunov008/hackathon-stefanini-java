package com.stefanini.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.stefanini.model.Tipoinfracao;
import com.stefanini.service.TipoinfracaoService;


@Named("tipoinfracaoMB")
@SessionScoped
public class TipoinfracaoBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -502260959536609412L;
	@Inject
    private TipoinfracaoService tipoinfracaoService;
	
	@Inject
	private Tipoinfracao tipoinfracao;
	
	private List<Tipoinfracao> tipoinfracaoList;
	
	public void chamarTipoinfracao() {
		tipoinfracaoService.inserirTipoinfracao(tipoinfracao);
		tipoinfracaoList.add(tipoinfracao);
		tipoinfracao = new Tipoinfracao();
    }

	public Tipoinfracao getTipoinfracao() {
		if(tipoinfracao == null){
			tipoinfracao = new Tipoinfracao();
		}
		return tipoinfracao;
	}

	public void setTipoinfracao(Tipoinfracao tipoinfracao) {
		this.tipoinfracao = tipoinfracao;
	}

	public List<Tipoinfracao> getTipoinfracaoList() {
		if(tipoinfracaoList == null){
			tipoinfracaoList = new ArrayList<Tipoinfracao>();
			tipoinfracaoList = tipoinfracaoService.listarTipoinfracao();
		}
		return tipoinfracaoList;
	}

	public void setTipoinfracaoList(List<Tipoinfracao> tipoinfracaoList) {
		this.tipoinfracaoList = tipoinfracaoList;
	}
}
