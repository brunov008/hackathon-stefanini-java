package com.stefanini.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.stefanini.model.Infracoes;
import com.stefanini.service.InfracoesService;

@Named("infracoesMB")
@SessionScoped
public class InfracoesBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8932480472811901801L;
	@Inject
    private InfracoesService infracoesService;
	
	@Inject
	private Infracoes infracoes;
	
	private List<Infracoes> infracoes1;
	
	public Infracoes getInfracoes() {
    	if(infracoes == null){
    		infracoes = new Infracoes();
    	}
		return infracoes;
	}
	
	public void setInfracoes(Infracoes infracoes) {
		this.infracoes = infracoes;
	}

	public List<Infracoes> getInfracoes1() {
		if(infracoes1 == null){
			infracoes1 = new ArrayList<Infracoes>();
			infracoes1 = infracoesService.Listarinfracoes();
		}
		return infracoes1;
	}

	public void setInfracoes1(List<Infracoes> infracoes1) {
		this.infracoes1 = infracoes1;
	}
	
	public void chamarInfracoes() {
		infracoesService.incluirInfracoes(infracoes);
		infracoes1.add(infracoes);
		infracoes = new Infracoes();
    }
	
}
