package com.stefanini.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.stefanini.model.Veiculos;
import com.stefanini.service.VeiculosService;

@Named("veiculosMB")
@SessionScoped
public class VeiculosBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1163194587529817245L;

	@Inject
    private VeiculosService veiculosService;
    
    @Inject
    private Veiculos veiculos;
    
    private List<Veiculos> veiculosList;
    
    public Veiculos getVeiculos() {
    	if(veiculos == null){
    		veiculos = new Veiculos();
    	}
		return veiculos;
	}


	public void chamarVeiculo() {
		veiculosService.incluirveiculos(veiculos);
		veiculosList.add(veiculos);
		veiculos = new Veiculos();
    }

	public void setVeiculos(Veiculos veiculos) {
		this.veiculos = veiculos;
	}


	public List<Veiculos> getVeiculosList() {
		if(veiculosList == null){
			veiculosList = new ArrayList<Veiculos>();
			veiculosList = veiculosService.listarVeiculos();
		}
		return veiculosList;
	}


	public void setVeiculosList(List<Veiculos> veiculosList) {
		this.veiculosList = veiculosList;
	}
}

	
