package com.stefanini.bean;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.stefanini.model.Tipotelefone;
import com.stefanini.service.TipotelefoneService;

@Named("tipotelefoneMB")
@SessionScoped
public class TipotelefoneBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7250945949090902960L;
	
	@Inject
	private TipotelefoneService tipotelefoneService;
	
	@Inject
	private Tipotelefone tipotelefone;

	public Tipotelefone getTipotelefone() {
		if(tipotelefone == null){
			tipotelefone = new Tipotelefone();
		}
		return tipotelefone;
	}
	
	public String chamarTipotelefone() {
        return "/pages/teste.faces?faces-redirect=true";
    }

	public void setTipotelefone(Tipotelefone tipotelefone) {
		this.tipotelefone = tipotelefone;
	}
}
