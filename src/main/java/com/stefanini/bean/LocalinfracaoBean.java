package com.stefanini.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;


import com.stefanini.model.Localinfracao;
import com.stefanini.service.LocalinfracaoService;

@Named("LocalinfracaoMB")
@SessionScoped
public class LocalinfracaoBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1459890128239876419L;
	@Inject
	private Localinfracao localinfracao;
	
	@Inject
	private LocalinfracaoService localinfracaoService;
	
	private List<Localinfracao> localinfracaoList;
	
	public void chamarLocalinfracao(){
		localinfracaoService.incluirLocalinfracao(localinfracao);
		localinfracaoList.add(localinfracao);
		localinfracao = new Localinfracao();
	}
	

	public Localinfracao getLocalinfracao() {
		if(localinfracao == null){
			localinfracao = new Localinfracao();
		}
		return localinfracao;
	}

	public void setLocalinfracao(Localinfracao localinfracao) {
		this.localinfracao = localinfracao;
	}

	public List<Localinfracao> getLocalinfracaoList() {
		if(localinfracaoList == null){
			localinfracaoList = new ArrayList<Localinfracao>();
			localinfracaoList = localinfracaoService.listarLocalinfracao();
		}
		return localinfracaoList;
	}

	public void setLocalinfracaoList(List<Localinfracao> localinfracaoList) {
		this.localinfracaoList = localinfracaoList;
	}
	
}
