package com.stefanini.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.stefanini.model.Modelo;
import com.stefanini.service.ModeloService;

@Named("modeloMB")
@SessionScoped
public class ModeloBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -157736797629908017L;
	@Inject
    private ModeloService moedeloService;
	
	@Inject
	private Modelo modelo;
	
	private List<Modelo> modelolist;
	
	public void chamarModelo() {
        moedeloService.incluirModelo(modelo);
        modelolist.add(modelo);
        modelo = new Modelo();
    }

	public Modelo getModelo() {
		if(modelo == null){
			modelo = new Modelo();
		}
		return modelo;
	}

	public void setModelo(Modelo modelo) {
		this.modelo = modelo;
	}

	public List<Modelo> getModelolist() {
		if(modelolist == null){
			modelolist = new ArrayList<Modelo>();
			modelolist = moedeloService.listarModelo();
		}
		return modelolist;
	}

	public void setModelolist(List<Modelo> modelolist) {
		this.modelolist = modelolist;
	}
}
