package com.stefanini.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.stefanini.model.Agente;
import com.stefanini.service.AgenteService;

@Named("agenteMB")
@SessionScoped
public class AgenteBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8627177097552094706L;
	
	@Inject
	private AgenteService agenteService;
	
	@Inject
	private Agente agente;
	
	private List<Agente> agentes;
	
	public Agente getAgente() {
		if(agente == null){
			agente = new Agente();
		}
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	public List<Agente> getAgentes() {
		if(agentes == null){
			agentes = new ArrayList<Agente>();
			agentes = agenteService.listarAgente();
		}
		return agentes;
	}

	public void setAgentes(List<Agente> agentes) {
		this.agentes = agentes;
	}
	
	public void chamarAgente() {
		agenteService.incluirAgente(agente);
		agentes.add(agente);
		agente = new Agente();
    }
	
	

}
