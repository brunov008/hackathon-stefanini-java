package com.stefanini.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.stefanini.model.Proprietario;
import com.stefanini.service.ProprietarioService;

@Named("propretarioMB")
@SessionScoped
public class ProprietarioBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2387270037716446355L;
	
	@Inject
	private ProprietarioService proprietarioservice;
	
	@Inject
	private Proprietario proprietario;
	
	private List<Proprietario> proprietarioList;
	
	public void chamarProprietario() {
       proprietarioservice.inserirProprietario(proprietario);
       proprietarioList.add(proprietario);
       proprietario = new Proprietario();
       
    }

	public Proprietario getProprietario() {
		if(proprietario == null){
			proprietario = new Proprietario();
		}
		return proprietario;
	}

	public void setProprietario(Proprietario proprietario) {
		this.proprietario = proprietario;
	}

	public List<Proprietario> getProprietarioList() {
		if(proprietarioList == null){
			proprietarioList = new ArrayList<Proprietario>();
			proprietarioList = proprietarioservice.listarProprietario();
		}
		return proprietarioList;
	}

	public void setProprietarioList(List<Proprietario> proprietarioList) {
		this.proprietarioList = proprietarioList;
	}

}
